#ifndef UR_H
#define UR_H

#include "stm32f1xx_hal.h"

class UltrasonicRanging
{
	private:
		TIM_HandleTypeDef* 	UR_TIM;
		uint32_t 						UR_TIM_Channel;
		GPIO_TypeDef* 			UR_GPIOx;
		uint16_t 						UR_PIN;
		uint16_t 						ValueCompare, range;
		TIM_IC_InitTypeDef 	sConfigIC;

	public:
		UltrasonicRanging(TIM_HandleTypeDef* UR_TIM, uint32_t UR_TIM_Channel, GPIO_TypeDef* UR_GPIOx, uint16_t UR_PIN);
		void UR_SaveCompare();
		uint16_t UR_GetRange();
		~UltrasonicRanging();
		
	protected:
};

#endif
