#include "UR_class.h"

//class UltrasonicRanging
//{
//	private:
//		TIM_HandleTypeDef* 	UR_TIM;
//		uint32_t 						UR_TIM_Channel;
//		GPIO_TypeDef* 			UR_GPIOx;
//		uint16_t 						UR_PIN;
//		uint16_t 						ValueCompare, range;
//		TIM_IC_InitTypeDef 	sConfigIC;

//	public:
		UltrasonicRanging::UltrasonicRanging(TIM_HandleTypeDef* UR_TIM_, uint32_t UR_TIM_Channel_, GPIO_TypeDef* UR_GPIOx_, uint16_t UR_PIN_)
		{
			this->UR_TIM = UR_TIM_;
			this->UR_TIM_Channel = UR_TIM_Channel_;
			this->UR_GPIOx = UR_GPIOx_;
			this->UR_PIN = UR_PIN_;
			
			sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
			sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
			sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
			sConfigIC.ICFilter = 5;
			HAL_TIM_IC_ConfigChannel(UR_TIM, &sConfigIC, UR_TIM_Channel);
			
			HAL_TIM_IC_Start_IT(UR_TIM, UR_TIM_Channel);
		}
		
		//================================================
		void UltrasonicRanging::UR_SaveCompare()
		{
			if (( (*UR_TIM).Instance->SR & ( 2 << (UR_TIM_Channel/4) )) == ( 2 << (UR_TIM_Channel/4) ))
			{
				(*UR_TIM).Instance->SR &= (uint16_t)~(uint16_t)(2<<(UR_TIM_Channel/4));
				if(HAL_GPIO_ReadPin(UR_GPIOx, UR_PIN))
				{
					ValueCompare = __HAL_TIM_GetCompare(UR_TIM, UR_TIM_Channel);
					sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
				}
				else
				{
					range = (__HAL_TIM_GetCompare(UR_TIM, UR_TIM_Channel) - ValueCompare) / 58;
					sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
				}
				HAL_TIM_IC_ConfigChannel(UR_TIM, &sConfigIC, UR_TIM_Channel);
			}
		}
		
		//================================================
		uint16_t UltrasonicRanging::UR_GetRange()
		{
			return range;
		}
		
		//================================================
		UltrasonicRanging::~UltrasonicRanging(){}
		
//	protected:
//};
